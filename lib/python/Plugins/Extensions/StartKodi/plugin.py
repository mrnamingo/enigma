# Embedded file name: /usr/lib/enigma2/python/Plugins/Extensions/StartKodi/plugin.py
from Screens.Screen import Screen
from Components.ActionMap import ActionMap
from Components.Label import Label
from Plugins.Plugin import PluginDescriptor
from Screens.MessageBox import MessageBox
import os
from enigma import quitMainloop
from Plugins.Extensions.StartKodi.installsomething import InstallSomething

class StartKodi2(Screen):
    kodi_name = 'kodiaml'
    kodineeds = 200
    caninstall = False
    isinstalled = False
    skin = '\n\t\t<screen position="center,center" size="500,200" title="Start Kodi">\n\t\t<widget name="text" position="30,30" size="360,25" font="Regular;25" />\n\t\t<widget name="sd_label" position="30,100" size="310,25" font="Regular;20" />\n\t\t<widget name="freespace_label" position="30,125" size="310,25" font="Regular;20" />\n\t\t<widget name="installed_label" position="30,150" size="310,25" font="Regular;20" />\n\t\t<widget name="sd" position="340,100" size="150,25" font="Regular;20" />\n\t\t<widget name="freespace" position="340,125" size="150,25" font="Regular;20" />\n\t\t<widget name="installed" position="340,150" size="150,25" font="Regular;20" />\n\t\t</screen>'

    def __init__(self, session, args = 0):
        self.session = session
        Screen.__init__(self, session)
        freembsd = str(self.getFreeSD())
        isInstalled = str(self.isKodiInstalled())
        self['text'] = Label(_('Please press OK to start Kodi...'))
        self['sd_label'] = Label(_('Kodi/extra partition free space:'))
        self['freespace_label'] = Label(_('System partition free space:'))
        self['installed_label'] = Label(_('Kodi installed:'))
        self['sd'] = Label(freembsd + ' MB')
        self['installed'] = Label(isInstalled)
        self['actions'] = ActionMap(['OkCancelActions'], {'ok': self.ok,
         'cancel': self.close})
        self.onShown.append(self.onFirstShown)

    def onFirstShown(self):
        self.onShown.remove(self.onFirstShown)
        if self.isinstalled:
            self['text'] = Label(_('\n   Please press OK to start Kodi...'))
        elif self.caninstall is False:
            self['text'] = Label(_('\n  WARNING: \n  There is not enough space to install Kodi!'))
        else:
            self.session.openWithCallback(self.doInstallCallback, MessageBox, _('\n Kodi not present. Proceed with install?'))

    def doInstallCallback(self, result):
        if result:
            self.KodiInstallation = InstallSomething(self.session, [self.kodi_name])
            self.KodiInstallation.__install__()
            self.isinstalled = True
            os.system('touch /etc/.kodistart')

    def ok(self):
        if self.isinstalled:
            os.system('touch /etc/.kodistart')
            quitMainloop(3)
        else:
            self.close()

    def getFreeNand(self):
        os.system('sync ; sync ; sync')
        sizeread = os.popen("df | grep %s | tr -s ' '" % 'mmcblk0p2')
        c = sizeread.read().strip().split(' ')
        sizeread.close()
        free = int(c[3]) / 1024
        if free > self.kodineeds:
            self.caninstall = True
        else:
            self.caninstall = False
        return free

    def getFreeSD(self):
        sizeread = os.popen("df | grep %s | tr -s ' '" % 'uSDextra')
        c = sizeread.read().strip().split(' ')
        sizeread.close()
        if os.path.exists('/media/uSDextra'):
            free = int(c[3]) / 1024
        else:
            free = 'Not available'
        return free

    def isKodiInstalled(self):
        if os.path.exists('/usr/bin/kodi'):
            self.isinstalled = True
            return True
        else:
            self.isinstalled = False
            return False


class SysMessage(Screen):
    skin = '\n\t\t<screen position="150,200" size="450,200" title="System Message" >\n\t\t\t<widget source="text" position="0,0" size="450,200" font="Regular;20" halign="center" valign="center" render="Label" />\n\t\t\t<ePixmap pixmap="icons/input_error.png" position="5,5" size="53,53" alphatest="on" />\n\t\t</screen>'

    def __init__(self, session, message):
        from Components.Sources.StaticText import StaticText
        Screen.__init__(self, session)
        self['text'] = StaticText(message)
        self['actions'] = ActionMap(['OkCancelActions'], {'cancel': self.ok})

    def ok(self):
        self.close()


def main(session, **kwargs):
    session.open(StartKodi2)


def menu(menuid, **kwargs):
    if menuid == 'mainmenu':
        return [(_('Start Kodi'),
          main,
          'start_kodi',
          44)]
    return []


def Plugins(**kwargs):
    return [PluginDescriptor(name=_('Start Kodi'), description=_('Play back media files'), where=PluginDescriptor.WHERE_MENU, needsRestart=False, fnc=menu)]